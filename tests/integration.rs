use dot_writer::*;

#[test]
fn test_trivial_graphs() {
    assert_eq!(
        DotWriter::write_string(|w| {
            w.graph().edge("a", "b");
        }),
        "graph{a--b;}"
    );
    assert_eq!(
        DotWriter::write_string(|w| {
            w.digraph().edge("a", "b");
        }),
        "digraph{a->b;}"
    );
}

#[test]
fn test_nested_subgraphs() {
    assert_eq!(
        DotWriter::write_string(|w| {
            w.graph().subgraph().cluster().subgraph();
        }),
        "graph{subgraph{subgraph cluster_0{subgraph{}}}}"
    )
}

#[test]
fn test_pretty_printing() {
    // NB \x20 is the ascii for a space (" ")
    // required to make the multiline string not strip the whitespace
    assert_eq!(
        DotWriter::write_string(|w| {
            w.set_pretty_print(true);
            let mut graph = w.graph();
            let mut sub = graph.subgraph();
            let mut cluster = sub.cluster();
            let mut sub_sub = cluster.subgraph();
            let a = sub_sub.node_named("a").id();
            let b = sub_sub.node_named("b").id();
            sub_sub.edge(a, b).attributes().set_color(Color::Red);
        }),
        "graph {\n\
         \x20 subgraph {\n\
         \x20   subgraph cluster_0 {\n\
         \x20     subgraph {\n\
         \x20       a;\n\
         \x20       b;\n\
         \x20       a -- b [color=red];\n\
         \x20     }\n\
         \x20   }\n\
         \x20 }\n\
         }\n"
    )
}
