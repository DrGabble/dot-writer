# Graphviz DOT Writer

This library is a (hopefully) ergonomic library for plotting graphs. It outputs the [Graphviz](https://www.graphviz.org/)
language [DOT](https://www.graphviz.org/doc/info/lang.html). Graphs written in DOT can then be easily
converted to SVG or other image formats using the Graphviz [dot executable](https://graphviz.org/doc/info/command.html).

The structs in this library leverage the Rust type system and lifetimes to ensure that it's harder to use them to construct an invalid DOT graph. It's important to note that this means you need
to make sure that child structs go out of scope before using their parents again. This is to make sure that
the [`Drop`] writes the closing brackets correctly.

## Getting started

Have a look at the API on [docs.rs](https://docs.rs/dot-writer) for examples and how to get coding.

## Non Goals

This library only writes DOT in a strongly typed way. It doesn't read DOT or render DOT into image files.

## Contributing

Contributions welcome, please feel free to open issues and pull requests. I've only just realised (7/11/22) that I hadn't enabled issues on the github repository, so my apologies to anyone who tried to post issues in the past! Drop me (DrGabble) a message if you have any questions on anything at all, always happy to help.